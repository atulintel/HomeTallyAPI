﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTEntities.Models
{
    public class NomineeModel
    {
        public int NomineeId { get; set; }
        public Nullable<int> NomineeFromTypeId { get; set; }
        public int NomineeFromId { get; set; }
        public string NomineeName { get; set; }
        public System.DateTime NomineeDOB { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
