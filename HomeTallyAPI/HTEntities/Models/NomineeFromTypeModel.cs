﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTEntities.Models
{
    public class NomineeFromTypeModel
    {
        public int NomineeFromTypeId { get; set; }
        public string NomineeFromTypeName { get; set; }
        public System.DateTime CreatedDate { get; set; }
    }
}
