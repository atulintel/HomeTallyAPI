﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTEntities.Models
{
    public class BankTransactionModel
    {
        public int BankTransactionId { get; set; }
        public System.DateTime TransactionDate { get; set; }
        public double Amount { get; set; }
        public int TransactionTypeId { get; set; }
        public string TransactionNo { get; set; }
        public string PlaceOfTransaction { get; set; }
        public string ReasonOfTransaction { get; set; }
        public string Comments { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

    }
}
