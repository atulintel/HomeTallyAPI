﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTEntities.Models
{
    public class BankAccountModel
    {
        public int BankAccountId { get; set; }
        public string BankName { get; set; }
        public string AccountNo { get; set; }
        public string BranchName { get; set; }
        public string IFSCCode { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public System.DateTime AccountCreatedDate { get; set; }
        public double BalanceOpening { get; set; }
        public double CurrentBalance { get; set; }
        public int AccountTypeId { get; set; }
        public double InterestRate { get; set; }
        public Nullable<System.DateTime> AccountClosingDate { get; set; }
        public string ClosingReason { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
