﻿using HTEntities.Models;
using HTService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using System.Net.Http.Formatting;

namespace HomeTallyAPI.Controllers
{
    public class UsersController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpGet]
        [Route("api/GetUser/{id?}")]
        public HttpResponseMessage GetUser(int? id = null)
        {
            try
            {
                List<UsersModel> userList = UserService.getUser(id);
                return Request.CreateResponse(HttpStatusCode.OK, userList.ToList());
            }
            catch (Exception ex)
            {
                Log.Error("Error in Get User", ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Problem Occurred", ex);
            }
        }


        [HttpGet]
        [Route("api/Authenticate/{UserName}/{Password}")]
        public HttpResponseMessage Authenticate(string UserName, string Password)
        {
            try
            {
                UsersModel valid = UserService.validateUser(UserName, Password);
                return Request.CreateResponse(HttpStatusCode.OK, valid);
            }
            catch (Exception ex)
            {
                Log.Error("Error in Authenticate", ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Problem Occurred", ex);
            }
        }

        [HttpGet]
        [Route("api/CheckUserAvailability/{UserName}/{EmailId}/")]
        public HttpResponseMessage CheckUserAvailability(string UserName, string EmailId)
        {
            try
            {
                bool isAvailable = UserService.CheckUserAvailability(UserName, EmailId);
                return Request.CreateResponse(HttpStatusCode.OK, isAvailable);
            }
            catch (Exception ex)
            {
                Log.Error("Error in Authenticate", ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Problem Occurred", ex);
            }
        }


        [HttpPost]
        [Route("api/Register/")]
        public HttpResponseMessage Register([FromBody]UsersModel model)
        {
            try
            {
                UsersModel register = UserService.Register(model);
                return Request.CreateResponse(HttpStatusCode.OK, register);
            }
            catch (Exception ex)
            {
                Log.Error("Error in Authenticate", ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Problem Occurred", ex);
            }
        }
    }
}
