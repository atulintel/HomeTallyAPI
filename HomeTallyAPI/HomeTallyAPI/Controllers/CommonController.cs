﻿using HTEntities.Models;
using HTService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;

namespace HomeTallyAPI.Controllers
{
    public class CommonController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpGet]
        [Route("api/AccountTypeList/")]
        public HttpResponseMessage GetAccountType()
        {
            try
            {
                List<AccountTypeModel> accountTypeList = CommonService.getAccountType();
                return Request.CreateResponse(HttpStatusCode.OK, accountTypeList.ToList());
            }
            catch (Exception ex)
            {
                Log.Error("Error in Get Account Type", ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Problem Occurred", ex);
            }
        }

        [HttpGet]
        [Route("api/NomineeFromTypeList/")]
        public HttpResponseMessage GetNomineeFromType()
        {
            try
            {
                List<NomineeFromTypeModel> nomineeFromTypeList = CommonService.getNomineeFromType();
                return Request.CreateResponse(HttpStatusCode.OK, nomineeFromTypeList.ToList());
            }
            catch (Exception ex)
            {
                Log.Error("Error in Get Account Type", ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Problem Occurred", ex);
            }
        }

        [HttpGet]
        [Route("api/TransactionTypeList/")]
        public HttpResponseMessage GetTransactionType()
        {
            try
            {
                List<TransactionTypeModel> transactionTypeList = CommonService.getTransactionType();
                return Request.CreateResponse(HttpStatusCode.OK, transactionTypeList.ToList());
            }
            catch (Exception ex)
            {
                Log.Error("Error in Get Account Type", ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Problem Occurred", ex);
            }
        }
    }
}
