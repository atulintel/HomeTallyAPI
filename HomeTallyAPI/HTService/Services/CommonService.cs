﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTDataLayer.Database;
using System.Data.SqlClient;
using HTEntities.Models;

namespace HTService.Services
{
    public static class CommonService
    {
        public static List<AccountTypeModel> getAccountType()
        {
            List<AccountTypeModel> AccountTypeList = new List<AccountTypeModel>();

            using (HomeTallyEntities context = new HomeTallyEntities())
            {
                AccountTypeList = context.Database.SqlQuery<AccountTypeModel>("ht_getAccountTypeList").ToList();
            }
            return AccountTypeList;
        }

        public static List<NomineeFromTypeModel> getNomineeFromType()
        {
            List<NomineeFromTypeModel> NomineeFromTypeList = new List<NomineeFromTypeModel>();

            using (HomeTallyEntities context = new HomeTallyEntities())
            {
                NomineeFromTypeList = context.Database.SqlQuery<NomineeFromTypeModel>("ht_getNomineeFromType").ToList();
            }
            return NomineeFromTypeList;
        }

        public static List<TransactionTypeModel> getTransactionType()
        {
            List<TransactionTypeModel> transactionTypeList = new List<TransactionTypeModel>();

            using (HomeTallyEntities context = new HomeTallyEntities())
            {
                transactionTypeList = context.Database.SqlQuery<TransactionTypeModel>("ht_getTransactionType").ToList();
            }
            return transactionTypeList;
        }


    }
}
