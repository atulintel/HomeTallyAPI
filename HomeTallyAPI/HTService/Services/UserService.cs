﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTDataLayer.Database;
using System.Data.SqlClient;
using HTEntities.Models;

namespace HTService.Services
{
    public static class UserService
    {
        public static List<UsersModel> getUser(int? id)
        {
            List<UsersModel> UserList = new List<UsersModel>();

            using (HomeTallyEntities context = new HomeTallyEntities())
            {
                SqlParameter idParam = new SqlParameter();
                idParam.ParameterName = "Id";
                idParam.Value = (object)id ?? DBNull.Value;
                idParam.DbType = System.Data.DbType.Int32;

                UserList = context.Database.SqlQuery<UsersModel>("ht_getUsers @Id", idParam).ToList();
            }
            return UserList;
        }

        public static UsersModel validateUser(string UserName, string Password)
        {
            UsersModel user = new UsersModel();
            using (HomeTallyEntities context = new HomeTallyEntities())
            {
                SqlParameter usernameParam = new SqlParameter();
                usernameParam.ParameterName = "UserName";
                usernameParam.Value = UserName;
                usernameParam.DbType = System.Data.DbType.String;

                SqlParameter passwordParam = new SqlParameter();
                passwordParam.ParameterName = "Password";
                passwordParam.Value = Password;
                passwordParam.DbType = System.Data.DbType.String;

                user = context.Database.SqlQuery<UsersModel>("ht_validateUser @UserName,@Password", usernameParam, passwordParam).FirstOrDefault();
            }

            return user;
        }

        public static bool CheckUserAvailability(string UserName, string EmailId)
        {
            int count = 0;
            using (HomeTallyEntities context = new HomeTallyEntities())
            {
                SqlParameter usernameParam = new SqlParameter();
                usernameParam.ParameterName = "UserName";
                usernameParam.Value = UserName;
                usernameParam.DbType = System.Data.DbType.String;

                SqlParameter emailIdParam = new SqlParameter();
                emailIdParam.ParameterName = "EmailId";
                emailIdParam.Value = EmailId;
                emailIdParam.DbType = System.Data.DbType.String;

                count = context.Database.SqlQuery<int>(
                    "ht_checkUserAvailable @UserName,@EmailId", usernameParam, emailIdParam).FirstOrDefault();
            }

            return count == 0;
        }

        public static UsersModel Register(UsersModel data)
        {
            UsersModel user = new UsersModel();
            using (HomeTallyEntities context = new HomeTallyEntities())
            {
                SqlParameter usernameParam = new SqlParameter();
                usernameParam.ParameterName = "UserName";
                usernameParam.Value = data.UserName;
                usernameParam.DbType = System.Data.DbType.String;

                SqlParameter fullNameParam = new SqlParameter();
                fullNameParam.ParameterName = "FullName";
                fullNameParam.Value = data.FullName;
                fullNameParam.DbType = System.Data.DbType.String;

                SqlParameter passwordParam = new SqlParameter();
                passwordParam.ParameterName = "Password";
                passwordParam.Value = data.Password;
                passwordParam.DbType = System.Data.DbType.String;

                SqlParameter emailIdParam = new SqlParameter();
                emailIdParam.ParameterName = "EmailId";
                emailIdParam.Value = (object)data.EmailId ?? DBNull.Value;
                emailIdParam.DbType = System.Data.DbType.String;

                SqlParameter photoParam = new SqlParameter();
                photoParam.ParameterName = "Photo";
                photoParam.Value = (object)data.Photo ?? DBNull.Value;
                photoParam.DbType = System.Data.DbType.String;

                SqlParameter contactNoParam = new SqlParameter();
                contactNoParam.ParameterName = "ContactNo";
                contactNoParam.Value = (object)data.ContactNo ?? DBNull.Value;
                contactNoParam.DbType = System.Data.DbType.String;

                user = context.Database.SqlQuery<UsersModel>(
                    "ht_registerUser @UserName,@FullName,@Password,@EmailId,@Photo,@ContactNo",
                    usernameParam, fullNameParam, passwordParam, emailIdParam, photoParam, contactNoParam).FirstOrDefault();
            }

            return user;
        }
    }
}
